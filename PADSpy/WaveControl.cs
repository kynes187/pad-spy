﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PAD;

namespace PADSpy
{
    public partial class WaveControl : UserControl
    {
        public int TotalCoins { get; private set; }
        public Wave Wave { get; set; }

        public WaveControl()
        {
            InitializeComponent();
        }

        private void BindWave(Wave w)
        {
            waveBox.Text = "Wave " + w.Sequence;
            int TotalCoins = 0;
            for (int i = 0; i < 5; i++)
            {
                string ctrlKey = string.Format("monsterControl{0}", i+1);
                Control[] ctrls = this.Controls.Find(ctrlKey, true);
                if (ctrls == null || ctrls.Length < 1) return;

                MonsterControl ctrl = (MonsterControl)ctrls[0];
                Monster m = null;
                if (i < w.Monsters.Count)
                {
                    m = w.Monsters[i];
                    TotalCoins += m.Coins;
                }
                ctrl.SetMonster(m);
            }
        }

        private void WaveControl_Load(object sender, EventArgs e)
        {
            BindWave(Wave);
        }
    }
}
