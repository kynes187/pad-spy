﻿using PAD;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PADSpy
{
    static class ImageCache
    {
        private static Dictionary<string, Image> mPathImageDict = new Dictionary<string, Image>();

        public static Image GetImage(string filePath)
        {
            if (!Path.IsPathRooted(filePath))
            {
                filePath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), filePath));
            }

            if (!mPathImageDict.ContainsKey(filePath))
            {
                mPathImageDict[filePath] = Image.FromFile(filePath);
            }

            return mPathImageDict[filePath];
        }
    }

    static class MonsterImageCache
    {
        private struct ImageKey
        {
            public string MonsterID;
            public int MonsterLevel;
        }

        #region Text Properties

        private static FontFamily Font
        {
            get
            {
                return new FontFamily("Arial");
            }
        }

        private static int FontSize
        {
            get
            {
                return 16;
            }
        }

        private static StringFormat StringFmt
        {
            get
            {
                StringFormat strFmt = new StringFormat();
                strFmt.Alignment = StringAlignment.Center;
                strFmt.LineAlignment = StringAlignment.Far;
                return strFmt;
            }
        }
        #endregion Text Properties

        private static Dictionary<ImageKey, Image> mImageDict = new Dictionary<ImageKey, Image>();

        public static Image GetImage(string monsterID, int monsterLevel)
        {
            ImageKey key;
            key.MonsterID = monsterID;
            key.MonsterLevel = monsterLevel;

            if (!mImageDict.ContainsKey(key))
            {
                mImageDict[key] = CreateMonsterImage(monsterID, monsterLevel);
            }

            return mImageDict[key];
        }

        private static string GetImagePath(string monsterId)
        {
            string path = Settings.Instance.MonsterImageFolder;
            return Path.Combine(path, monsterId + ".png");
        }

        private static Image GetMonsterImage(string monsterId)
        {
            string imageFile;
            try
            {
                imageFile = GetImagePath(monsterId);
                return ImageCache.GetImage(imageFile);
            }
            catch {}

            try
            {
                imageFile = GetImagePath("0");
                return ImageCache.GetImage(imageFile);
            }
            catch { }

            return null;
        }

        private static Image CreateMonsterImage(string monsterID, int monsterLevel)
        {
            Image img = GetMonsterImage(monsterID);

            if (img == null) return null;

            string levelText = string.Format("Lv. {0}", monsterLevel);
            return DrawText(img, levelText);
        }

        private static Image DrawText(Image img, string text)
        {
            Graphics g = Graphics.FromImage(img);
            g.InterpolationMode = InterpolationMode.High;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            g.CompositingQuality = CompositingQuality.HighQuality;

            GraphicsPath strPath = GetStringPath(img, text);
            // Outline
            g.DrawPath(new Pen(Brushes.Black, 2), strPath);
            // Fill
            g.FillPath(Brushes.White, strPath);
            return img;
        }


        private static GraphicsPath GetStringPath(Image img, string text)
        {
            GraphicsPath gPath = new GraphicsPath();
            Rectangle r = new Rectangle(0, 0, img.Width, img.Height);
            gPath.AddString(text, Font, (int)FontStyle.Bold, FontSize, r, StringFmt);
            return gPath;
        }
    }
}
