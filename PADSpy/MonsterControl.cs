﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PAD;
using System.IO;

namespace PADSpy
{
    public partial class MonsterControl : UserControl
    {
        public void SetMonster(Monster m)
        {
            BindMonster(m);
        }

        public MonsterControl()
        {
            InitializeComponent();
        }

        private void BindMonster(Monster m)
        {
            if (m == null) return;

            monsterPic.Visible = (m.MonsterID != Monster.DefaultMonsterNumber);
            if (monsterPic.Visible)
            {
                monsterPic.Image = MonsterImageCache.GetImage(m.MonsterID, m.MonsterLevel) ?? monsterPic.ErrorImage;
            }

            dropPic.Visible = (m.DropMonsterID != Monster.DefaultMonsterNumber);
            if (dropPic.Visible)
            {
                dropPic.Image = MonsterImageCache.GetImage(m.MonsterID, m.MonsterLevel) ?? dropPic.ErrorImage;
            }
            
            lblCoins.Visible = (m.Coins > 0);
            if (lblCoins.Visible)
            {
                lblCoins.Text = string.Format("{0} coins", m.Coins);
            }
        }
    }
}
