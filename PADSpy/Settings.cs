﻿using SharpPcap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace PADSpy
{
    [DataContract]
    public class Settings
    {
        [DataMember]
        public string MonsterImageFolder { get; set; }
        [DataMember]
        public int CaptureTimeoutMilliseconds { get; set; }
        
        public HashSet<ICaptureDevice> EnabledCaptureDevices { get; set; }

        [DataMember]
        public int RefreshCaptureStatsMilliseconds { get; set; }

        [DataMember]
        private string[] mCaptureDeviceNames = new string[0];


        private static Settings Instance_ = null;
        public static Settings Instance
        {
            get
            {
                if (Instance_ == null)
                {
                    if (File.Exists(FileName))
                    {
                        Instance_ = FromDisk();
                    }
                    else
                    {
                        Instance_ = new Settings();
                    }
                }

                return Instance_;
            }
        }

        private static readonly string DirName = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "PADSpy");
        private static readonly string FileName = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "PADSpy", "Settings.json");
        private static readonly XmlObjectSerializer Serializer = new DataContractJsonSerializer(typeof(Settings));

        public static void Save()
        {
            Instance.SaveCaptureDeviceNames();

            if (!Directory.Exists(DirName))
            {
                Directory.CreateDirectory(DirName);
            }

            using (FileStream fStream = new FileStream(FileName, System.IO.FileMode.Create))
            {
                Serializer.WriteObject(fStream, Instance);
            }
        }

        private static Settings FromDisk()
        {
            using (FileStream fStream = new FileStream(FileName, FileMode.Open))
            {
                Settings s = (Settings)Serializer.ReadObject(fStream);
                s.ReadCaptureDeviceNames();
                return s;
            }
        }

        private Settings()
        {
            MonsterImageFolder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "thumbnails");
            CaptureTimeoutMilliseconds = 1000;
            RefreshCaptureStatsMilliseconds = 1000;
            EnabledCaptureDevices = new HashSet<ICaptureDevice>();
        }

        private void SaveCaptureDeviceNames()
        {
            mCaptureDeviceNames = EnabledCaptureDevices.Select<ICaptureDevice, string>( d => d.Name).ToArray<string>();
        }

        private void ReadCaptureDeviceNames()
        {
            IEnumerable<ICaptureDevice> devices = mCaptureDeviceNames.Select<string, ICaptureDevice>(s => CaptureDeviceList.Instance[s]);
            EnabledCaptureDevices = new HashSet<ICaptureDevice>(devices);
        }
    }
}
