﻿using SharpPcap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PADSpy
{
    public partial class OptionsForm : Form
    {
        private class BindableCaptureDevice
        {
            public ICaptureDevice Device { get; set; }
            public string Name { get; set; }

            public BindableCaptureDevice(ICaptureDevice device)
            {
                Device = device;
                Name = FormatDeviceName(device);
            }

            private static string FormatDeviceName(ICaptureDevice device)
            {
                string name;
                SharpPcap.LibPcap.PcapDevice pcapDevice = device as SharpPcap.LibPcap.PcapDevice;
                if (pcapDevice == null || pcapDevice.Interface == null || String.IsNullOrEmpty(pcapDevice.Interface.FriendlyName))
                {
                    name = device.Name;
                }
                else
                {
                    name = pcapDevice.Interface.FriendlyName;
                }

                return name;
            }
        }

        private void BindCaptureDevices()
        {
            List<BindableCaptureDevice> deviceList = new List<BindableCaptureDevice>();

            // Build a list of device and name pairs, using friendly names when possible
            foreach (ICaptureDevice device in CaptureDeviceList.Instance)
            {
                deviceList.Add(new BindableCaptureDevice(device));
            }

            captureDeviceCheckList.Items.Clear();
            captureDeviceCheckList.DataSource = deviceList;
            captureDeviceCheckList.DisplayMember = "Name";
            captureDeviceCheckList.ValueMember = "Device";

            SetSelectedDevices(Settings.Instance.EnabledCaptureDevices);
        }

       

        private void SetSelectedDevices(ICollection<ICaptureDevice> selectedDevices)
        {
            for (int i = 0; i < captureDeviceCheckList.Items.Count; i++)
            {
                BindableCaptureDevice device = captureDeviceCheckList.Items[i] as BindableCaptureDevice;

                if (device == null || device.Device == null) continue;

                bool isSelected = selectedDevices.Contains(device.Device);
                captureDeviceCheckList.SetItemChecked(i, isSelected);
            }
        }

        private void BindSettings()
        {
            txtMonsterImagesFolder.Text = Settings.Instance.MonsterImageFolder;

            BindCaptureDevices();
        }

        public void SaveSettings()
        {
            Settings.Instance.MonsterImageFolder = txtMonsterImagesFolder.Text;

            HashSet<ICaptureDevice> deviceSet = new HashSet<ICaptureDevice>();
            foreach (BindableCaptureDevice bindDevice in captureDeviceCheckList.CheckedItems)
            {
                deviceSet.Add(bindDevice.Device);
            }
            Settings.Instance.EnabledCaptureDevices = deviceSet;
        }

        public OptionsForm()
        {
            InitializeComponent();
            this.Load += OptionsForm_Load;
        }

        void OptionsForm_Load(object sender, EventArgs e)
        {
            BindSettings();
        }

        private void btnBrowseImageFolder_Click(object sender, EventArgs e)
        {
            if (monsterImagesFolderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtMonsterImagesFolder.Text = monsterImagesFolderBrowser.SelectedPath;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void captureDeviceCheckList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idx = captureDeviceCheckList.SelectedIndex;

            if (idx < 0) return;

            bool isChecked = captureDeviceCheckList.GetItemChecked(idx);
            captureDeviceCheckList.SetItemChecked(idx, !isChecked);
        }
    }
}
