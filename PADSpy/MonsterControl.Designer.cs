﻿namespace PADSpy
{
    partial class MonsterControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monsterPic = new System.Windows.Forms.PictureBox();
            this.dropPic = new System.Windows.Forms.PictureBox();
            this.lblCoins = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.monsterPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropPic)).BeginInit();
            this.SuspendLayout();
            // 
            // monsterPic
            // 
            this.monsterPic.Location = new System.Drawing.Point(3, 3);
            this.monsterPic.Name = "monsterPic";
            this.monsterPic.Size = new System.Drawing.Size(60, 60);
            this.monsterPic.TabIndex = 3;
            this.monsterPic.TabStop = false;
            // 
            // dropPic
            // 
            this.dropPic.Location = new System.Drawing.Point(3, 69);
            this.dropPic.Name = "dropPic";
            this.dropPic.Size = new System.Drawing.Size(60, 60);
            this.dropPic.TabIndex = 4;
            this.dropPic.TabStop = false;
            // 
            // lblCoins
            // 
            this.lblCoins.Location = new System.Drawing.Point(3, 69);
            this.lblCoins.Name = "lblCoins";
            this.lblCoins.Size = new System.Drawing.Size(60, 60);
            this.lblCoins.TabIndex = 5;
            this.lblCoins.Text = "Coins";
            this.lblCoins.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCoins.Visible = false;
            // 
            // MonsterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblCoins);
            this.Controls.Add(this.monsterPic);
            this.Controls.Add(this.dropPic);
            this.Name = "MonsterControl";
            this.Size = new System.Drawing.Size(66, 143);
            ((System.ComponentModel.ISupportInitialize)(this.monsterPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropPic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox monsterPic;
        private System.Windows.Forms.PictureBox dropPic;
        private System.Windows.Forms.Label lblCoins;
    }
}
