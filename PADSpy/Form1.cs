﻿using PAD;
using SharpPcap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Design.PluralizationServices;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PADSpy
{
    public partial class Form1 : Form
    {
        private enum ButtonCaptureState
        {
            Begin,
            Stop
        }

        public Form1()
        {
            InitializeComponent();
            // This should bind a list of identifiers in the future so the GUI doesn't
            // have to know so much about the various capture code.
            Sniffer.Instance.OnPacketMatched += Instance_PacketMatched;
            UpdateCaptureStats();
            UpdateDeviceMonitoring();
        }

        void Instance_PacketMatched(object sender, Dungeon dungeon)
        {
            DungeonForm d = new DungeonForm(dungeon);
            d.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Sniffer.Instance.Close();
            Settings.Save();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openCapFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    var captureFile = CaptureFileHelper.LoadCaptureFile(openCapFileDialog.FileName);
                    Sniffer.Instance.BeginCapture(captureFile, Settings.Instance.CaptureTimeoutMilliseconds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Failed to Load Capture file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionsForm form = new OptionsForm();
            form.FormClosed += optionsForm_FormClosed;
            form.Show();
        }

        void optionsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            OptionsForm form = (OptionsForm)sender;
            if (form.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                form.SaveSettings();
                UpdateDeviceMonitoring();
            }
        }

        private void timerCheckStats_Tick(object sender, EventArgs e)
        {
            UpdateCaptureStats();
        }

        private void UpdateDeviceMonitoring()
        {
            Sniffer.Instance.SetCaptureDevices(Settings.Instance.EnabledCaptureDevices, Settings.Instance.CaptureTimeoutMilliseconds);

            int count = Settings.Instance.EnabledCaptureDevices.Count<ICaptureDevice>( d => !(d is SharpPcap.LibPcap.CaptureFileReaderDevice));
            string deviceWord = "Device";
            if (count != 1)
            {
                deviceWord = PluralizationService.CreateService(CultureInfo.CurrentCulture).Pluralize(deviceWord);
            }

            lblMonitoredDeviceCount.Text = string.Format("Monitoring {0} Capture {1}", count.ToString(), deviceWord);

            if (count > 0)
            {
                timerCheckStats.Interval = Settings.Instance.RefreshCaptureStatsMilliseconds;
                timerCheckStats.Start();
            }
            else
            {
                timerCheckStats.Stop();
            }
        }

        private void UpdateCaptureStats()
        {
            uint totalReceived = 0;
            uint totalDropped = 0;
            foreach (ICaptureStatistics stats in Sniffer.Instance.GetStatistics())
            {
                totalReceived += stats.ReceivedPackets;
                totalDropped += stats.DroppedPackets;
            }

            lblPacketsReceivedCount.Text = totalReceived.ToString();
            lblPacketsDroppedCount.Text = totalDropped.ToString();
        }
    }
}
