﻿namespace PADSpy
{
    partial class DungeonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTotalCoins = new System.Windows.Forms.Label();
            this.lblCoinCount = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // lblTotalCoins
            // 
            this.lblTotalCoins.AutoSize = true;
            this.lblTotalCoins.Location = new System.Drawing.Point(12, 9);
            this.lblTotalCoins.Name = "lblTotalCoins";
            this.lblTotalCoins.Size = new System.Drawing.Size(63, 13);
            this.lblTotalCoins.TabIndex = 2;
            this.lblTotalCoins.Text = "Total Coins:";
            // 
            // lblCoinCount
            // 
            this.lblCoinCount.AutoSize = true;
            this.lblCoinCount.Location = new System.Drawing.Point(78, 9);
            this.lblCoinCount.Name = "lblCoinCount";
            this.lblCoinCount.Size = new System.Drawing.Size(52, 13);
            this.lblCoinCount.TabIndex = 2;
            this.lblCoinCount.Text = "XXX,XXX";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1, 25);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(488, 745);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // DungeonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 771);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.lblCoinCount);
            this.Controls.Add(this.lblTotalCoins);
            this.MinimumSize = new System.Drawing.Size(505, 242);
            this.Name = "DungeonForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dungeon View";
            this.Load += new System.EventHandler(this.DungeonForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTotalCoins;
        private System.Windows.Forms.Label lblCoinCount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;


    }
}