﻿namespace PADSpy
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMonsterImagesFolder = new System.Windows.Forms.TextBox();
            this.lblImagePath = new System.Windows.Forms.Label();
            this.btnBrowseImageFolder = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbGeneralSettings = new System.Windows.Forms.GroupBox();
            this.monsterImagesFolderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.gbCaptureDevices = new System.Windows.Forms.GroupBox();
            this.captureDeviceCheckList = new System.Windows.Forms.CheckedListBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbGeneralSettings.SuspendLayout();
            this.gbCaptureDevices.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtMonsterImagesFolder
            // 
            this.txtMonsterImagesFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMonsterImagesFolder.Location = new System.Drawing.Point(3, 5);
            this.txtMonsterImagesFolder.Name = "txtMonsterImagesFolder";
            this.txtMonsterImagesFolder.Size = new System.Drawing.Size(196, 20);
            this.txtMonsterImagesFolder.TabIndex = 0;
            // 
            // lblImagePath
            // 
            this.lblImagePath.AutoSize = true;
            this.lblImagePath.Location = new System.Drawing.Point(2, 8);
            this.lblImagePath.Name = "lblImagePath";
            this.lblImagePath.Size = new System.Drawing.Size(114, 13);
            this.lblImagePath.TabIndex = 1;
            this.lblImagePath.Text = "Monster Images Folder";
            // 
            // btnBrowseImageFolder
            // 
            this.btnBrowseImageFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseImageFolder.Location = new System.Drawing.Point(205, 3);
            this.btnBrowseImageFolder.Name = "btnBrowseImageFolder";
            this.btnBrowseImageFolder.Size = new System.Drawing.Size(26, 23);
            this.btnBrowseImageFolder.TabIndex = 2;
            this.btnBrowseImageFolder.Text = "...";
            this.btnBrowseImageFolder.UseVisualStyleBackColor = true;
            this.btnBrowseImageFolder.Click += new System.EventHandler(this.btnBrowseImageFolder_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.lblImagePath);
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(122, 35);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.txtMonsterImagesFolder);
            this.panel2.Controls.Add(this.btnBrowseImageFolder);
            this.panel2.Location = new System.Drawing.Point(131, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(239, 35);
            this.panel2.TabIndex = 4;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(252, 206);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(65, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(323, 206);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // gbGeneralSettings
            // 
            this.gbGeneralSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbGeneralSettings.Controls.Add(this.panel1);
            this.gbGeneralSettings.Controls.Add(this.panel2);
            this.gbGeneralSettings.Location = new System.Drawing.Point(12, 7);
            this.gbGeneralSettings.Name = "gbGeneralSettings";
            this.gbGeneralSettings.Size = new System.Drawing.Size(376, 60);
            this.gbGeneralSettings.TabIndex = 6;
            this.gbGeneralSettings.TabStop = false;
            this.gbGeneralSettings.Text = "General Settings";
            // 
            // gbCaptureDevices
            // 
            this.gbCaptureDevices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCaptureDevices.Controls.Add(this.captureDeviceCheckList);
            this.gbCaptureDevices.Location = new System.Drawing.Point(12, 74);
            this.gbCaptureDevices.Name = "gbCaptureDevices";
            this.gbCaptureDevices.Size = new System.Drawing.Size(376, 125);
            this.gbCaptureDevices.TabIndex = 7;
            this.gbCaptureDevices.TabStop = false;
            this.gbCaptureDevices.Text = "Capture Devices";
            // 
            // captureDeviceCheckList
            // 
            this.captureDeviceCheckList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.captureDeviceCheckList.FormattingEnabled = true;
            this.captureDeviceCheckList.Location = new System.Drawing.Point(11, 19);
            this.captureDeviceCheckList.Name = "captureDeviceCheckList";
            this.captureDeviceCheckList.Size = new System.Drawing.Size(351, 94);
            this.captureDeviceCheckList.TabIndex = 0;
            this.captureDeviceCheckList.SelectedIndexChanged += new System.EventHandler(this.captureDeviceCheckList_SelectedIndexChanged);
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(400, 238);
            this.Controls.Add(this.gbCaptureDevices);
            this.Controls.Add(this.gbGeneralSettings);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Name = "OptionsForm";
            this.ShowIcon = false;
            this.Text = "PAD Spy Options";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbGeneralSettings.ResumeLayout(false);
            this.gbCaptureDevices.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtMonsterImagesFolder;
        private System.Windows.Forms.Label lblImagePath;
        private System.Windows.Forms.Button btnBrowseImageFolder;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox gbGeneralSettings;
        private System.Windows.Forms.FolderBrowserDialog monsterImagesFolderBrowser;
        private System.Windows.Forms.GroupBox gbCaptureDevices;
        private System.Windows.Forms.CheckedListBox captureDeviceCheckList;
    }
}