﻿namespace PADSpy
{
    partial class WaveControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.waveBox = new System.Windows.Forms.GroupBox();
            this.lblMonsters = new System.Windows.Forms.Label();
            this.lblDrops = new System.Windows.Forms.Label();
            this.monsterControl5 = new PADSpy.MonsterControl();
            this.monsterControl4 = new PADSpy.MonsterControl();
            this.monsterControl3 = new PADSpy.MonsterControl();
            this.monsterControl2 = new PADSpy.MonsterControl();
            this.monsterControl1 = new PADSpy.MonsterControl();
            this.waveBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // waveBox
            // 
            this.waveBox.Controls.Add(this.monsterControl5);
            this.waveBox.Controls.Add(this.monsterControl4);
            this.waveBox.Controls.Add(this.monsterControl3);
            this.waveBox.Controls.Add(this.monsterControl2);
            this.waveBox.Controls.Add(this.monsterControl1);
            this.waveBox.Controls.Add(this.lblMonsters);
            this.waveBox.Controls.Add(this.lblDrops);
            this.waveBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.waveBox.Location = new System.Drawing.Point(0, 0);
            this.waveBox.Name = "waveBox";
            this.waveBox.Size = new System.Drawing.Size(452, 180);
            this.waveBox.TabIndex = 5;
            this.waveBox.TabStop = false;
            this.waveBox.Text = "groupBox1";
            // 
            // lblMonsters
            // 
            this.lblMonsters.AutoSize = true;
            this.lblMonsters.Location = new System.Drawing.Point(21, 45);
            this.lblMonsters.Name = "lblMonsters";
            this.lblMonsters.Size = new System.Drawing.Size(50, 13);
            this.lblMonsters.TabIndex = 13;
            this.lblMonsters.Text = "Monsters";
            // 
            // lblDrops
            // 
            this.lblDrops.AutoSize = true;
            this.lblDrops.Location = new System.Drawing.Point(36, 114);
            this.lblDrops.Name = "lblDrops";
            this.lblDrops.Size = new System.Drawing.Size(35, 13);
            this.lblDrops.TabIndex = 14;
            this.lblDrops.Text = "Drops";
            // 
            // monsterControl5
            // 
            this.monsterControl5.AutoSize = true;
            this.monsterControl5.Location = new System.Drawing.Point(365, 21);
            this.monsterControl5.Name = "monsterControl5";
            this.monsterControl5.Size = new System.Drawing.Size(66, 152);
            this.monsterControl5.TabIndex = 15;
            // 
            // monsterControl4
            // 
            this.monsterControl4.AutoSize = true;
            this.monsterControl4.Location = new System.Drawing.Point(293, 21);
            this.monsterControl4.Name = "monsterControl4";
            this.monsterControl4.Size = new System.Drawing.Size(66, 152);
            this.monsterControl4.TabIndex = 16;
            // 
            // monsterControl3
            // 
            this.monsterControl3.AutoSize = true;
            this.monsterControl3.Location = new System.Drawing.Point(221, 21);
            this.monsterControl3.Name = "monsterControl3";
            this.monsterControl3.Size = new System.Drawing.Size(66, 152);
            this.monsterControl3.TabIndex = 17;
            // 
            // monsterControl2
            // 
            this.monsterControl2.AutoSize = true;
            this.monsterControl2.Location = new System.Drawing.Point(149, 21);
            this.monsterControl2.Name = "monsterControl2";
            this.monsterControl2.Size = new System.Drawing.Size(66, 152);
            this.monsterControl2.TabIndex = 18;
            // 
            // monsterControl1
            // 
            this.monsterControl1.AutoSize = true;
            this.monsterControl1.Location = new System.Drawing.Point(77, 21);
            this.monsterControl1.Name = "monsterControl1";
            this.monsterControl1.Size = new System.Drawing.Size(66, 152);
            this.monsterControl1.TabIndex = 19;
            // 
            // WaveControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.waveBox);
            this.Name = "WaveControl";
            this.Size = new System.Drawing.Size(452, 180);
            this.Load += new System.EventHandler(this.WaveControl_Load);
            this.waveBox.ResumeLayout(false);
            this.waveBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox waveBox;
        private MonsterControl monsterControl5;
        private MonsterControl monsterControl4;
        private MonsterControl monsterControl3;
        private MonsterControl monsterControl2;
        private MonsterControl monsterControl1;
        private System.Windows.Forms.Label lblMonsters;
        private System.Windows.Forms.Label lblDrops;

    }
}
