﻿using PAD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PADSpy
{
    public partial class DungeonForm : Form
    {
        private Dungeon mDungeon;

        public DungeonForm(Dungeon dungeon)
        {
            InitializeComponent();
            mDungeon = dungeon;
        }

        private void BindDungeon(Dungeon d)
        {
            foreach (Wave w in d.Waves)
            {
                WaveControl ctrl = new WaveControl();
                ctrl.Wave = w;
                flowLayoutPanel1.Controls.Add(ctrl);
            }

            lblCoinCount.Text = string.Format("{0:G}", d.TotalCoins);
        }

        private void DungeonForm_Load(object sender, EventArgs e)
        {
            BindDungeon(mDungeon);
        }
    }
}
