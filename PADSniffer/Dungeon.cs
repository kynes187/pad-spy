﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PAD
{
    [DataContract]
    public class Dungeon
    {
        [DataMember(Name = "waves")]
        private List<Wave> mWaves = new List<Wave>();
        public List<Wave> Waves
        {
            get
            {
                return mWaves;
            }
        }

        public int TotalCoins
        {
            get
            {
                return Waves.Sum<Wave>(w => w.TotalCoins);
            }
        }
    }

    [DataContract]
    public class Wave
    {
        [DataMember(Name = "seq")]
        public string Sequence { get; set; }

        [DataMember(Name = "monsters")]
        private List<Monster> mMonsters = new List<Monster>();
        public List<Monster> Monsters
        {
            get
            {
                return mMonsters;
            }

            set
            {
                mMonsters = value;
            }
        }

        public int TotalCoins
        {
            get
            {
                return mMonsters.Sum<Monster>(m => m.Coins);
            }
        }
    }

    [DataContract]
    public class Monster
    {
        private static string CoinItemID = "900";
        public static string DefaultMonsterNumber = "0";

        [DataMember(Name = "type")]
        public int Type { get; private set; }

        [DataMember(Name = "num")]
        public string MonsterID { get; private set; }

        [DataMember(Name = "lv")]
        public int MonsterLevel { get; private set; }

        [DataMember(Name = "item")]
        private string mItemID = DefaultMonsterNumber;

        public string DropMonsterID
        {
            get
            {
                if (mItemID == CoinItemID)
                {
                    return DefaultMonsterNumber;
                }

                return mItemID;
            }
        }

        public int DropMonsterLevel
        {
            get
            {
                if (DropMonsterID == DefaultMonsterNumber)
                {
                    return 0;
                }

                return mItemNumber;
            }
        }

        [DataMember(Name = "inum")]
        private int mItemNumber = 0;

        public int Coins
        {
            get
            {
                if (mItemID == CoinItemID)
                {
                    return mItemNumber;
                }

                return 0;
            }
        }

        public Monster() { }
    }
}
