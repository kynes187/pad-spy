﻿using SharpPcap.LibPcap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAD
{
    public static class CaptureFileHelper
    {
        public static CaptureFileReaderDevice LoadCaptureFile(string fileName)
        {
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("Unable to load capture file. File not found.", fileName);
            }

            return new CaptureFileReaderDevice(fileName);
        }
    }
}
