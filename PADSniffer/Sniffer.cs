﻿using PacketDotNet;
using SharpPcap;
using SharpPcap.LibPcap;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace PAD
{
    public class Sniffer
    {
        private static Sniffer instance = null;
        public static Sniffer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Sniffer();
                }

                return instance;
            }
        }

        private Sniffer()
        {

        }

        private ConcurrentDictionary<ICaptureDevice, ICaptureStatistics> mCapturingDevices = new ConcurrentDictionary<ICaptureDevice, ICaptureStatistics>();
        public bool IsCapturing(ICaptureDevice device)
        {
            return mCapturingDevices.ContainsKey(device);
        }

        public delegate void MatchedPacketHandler(object sender, Dungeon dungeon);
        public event MatchedPacketHandler OnPacketMatched;

        #region Public Methods

        public void SetCaptureDevices(IEnumerable<ICaptureDevice> enabledDevices, int timeout)
        {
            HashSet<ICaptureDevice> disabledDevices = new HashSet<ICaptureDevice>(CaptureDeviceList.Instance);

            // Begin devices that are checked
            foreach (ICaptureDevice device in enabledDevices)
            {
                PAD.Sniffer.Instance.BeginCapture(device, timeout);
                disabledDevices.Remove(device);
            }

            // Stop devices that are not checked
            foreach (ICaptureDevice device in disabledDevices)
            {
                PAD.Sniffer.Instance.StopCapture(device);
            }
        }

        public void BeginCapture(ICaptureDevice device, int timeOutMilliseconds)
        {
            if (mCapturingDevices.ContainsKey(device)) return;

            device.OnPacketArrival += device_OnPacketArrival;

            if (!(device is CaptureFileReaderDevice))
            {
                device.Open(DeviceMode.Promiscuous, timeOutMilliseconds);
            }
            UpdateStatistics(device);
            device.Filter = "ip and tcp";
            device.StartCapture();

        }

        public void StopCapture(ICaptureDevice device)
        {
            if (mCapturingDevices.ContainsKey(device))
            {
                device.StopCapture();
                device.OnPacketArrival -= device_OnPacketArrival;
                ICaptureStatistics stats;
                mCapturingDevices.TryRemove(device, out stats);
            }
        }

        public void Close()
        {
            foreach (ICaptureDevice device in mCapturingDevices.Keys)
            {
                device.StopCapture();
                device.Close();
            }
            mCapturingDevices.Clear();
        }

        public List<ICaptureStatistics> GetStatistics()
        {
            List<ICaptureStatistics> statsList = new List<ICaptureStatistics>(mCapturingDevices.Count);
            foreach (ICaptureDevice device in mCapturingDevices.Keys)
            {
                if (device is CaptureFileReaderDevice) continue;

                ICaptureStatistics stats;
                mCapturingDevices.TryGetValue(device, out stats);
                statsList.Add(stats);
            }

            return statsList;
        }

        #endregion Public Methods


        private void UpdateStatistics(ICaptureDevice device)
        {
            if (device is CaptureFileReaderDevice) return;

            mCapturingDevices.AddOrUpdate(device, device.Statistics, (key, oldStats) => device.Statistics);
        }

        #region Packet Processing

        private ConcurrentQueue<RawCapture> mPacketQueue = new ConcurrentQueue<RawCapture>();
        private ConcurrentDictionary<Task, object> mPacketTasks = new ConcurrentDictionary<Task, object>();
        private int mMaxPacketTasks = 4;

        private void device_OnPacketArrival(object sender, CaptureEventArgs e)
        {
            if (OnPacketMatched == null) return;
            EnqueuePacket(e.Packet);
            UpdateStatistics(e.Device);
        }

        private void EnqueuePacket(RawCapture p)
        {
            mPacketQueue.Enqueue(p);
            CheckPacketQueue();
        }

        private void CheckPacketQueue()
        {
            if (mPacketTasks.Count < mMaxPacketTasks)
            {
                RawCapture cap;
                if (!mPacketQueue.TryDequeue(out cap)) return;

                Task t = new Task(() => ProcessPacket(cap));
                t.ContinueWith(task => PacketTaskComplete(task));
                mPacketTasks.TryAdd(t, null);
                t.Start();
            }
        }

        private void PacketTaskComplete(Task t)
        {
            object val;
            mPacketTasks.TryRemove(t, out val);
            CheckPacketQueue();
        }

        private void ProcessPacket(RawCapture cap)
        {
            Packet p = Packet.ParsePacket(cap.LinkLayerType, cap.Data);
            Dungeon d = SearchForDungeon(p);
            if (d != null)
            {
                RaiseEventOnUIThread(OnPacketMatched, this, d);
            }
        }

        private Dungeon SearchForDungeon(Packet p)
        {
            TcpPacket tcp = SearchForTcpPacket(p);
            if (tcp == null) return null;

            if (tcp.PayloadData != null && tcp.PayloadData.Length > 0)
            {
                try
                {
                    string json = GetJsonFromPayload(tcp.PayloadData);

                    if (json.Contains("waves"))//json.StartsWith("{\"res"))
                    {
                        try
                        {
                            DataContractJsonSerializer jsSerializer = new DataContractJsonSerializer(typeof(Dungeon));//, new Type[] { typeof(Wave), typeof(Monster) });
                            byte[] byteArr = Encoding.UTF8.GetBytes(json);
                            using (MemoryStream jsonStream = new MemoryStream(byteArr))
                            {
                                return (Dungeon)jsSerializer.ReadObject(jsonStream);
                            }
                        }
                        catch { }
                    }
                }
                catch { }
            }

            return null;
        }

        private string GetJsonFromPayload(byte[] payloadData)
        {
            string json = string.Empty;
            HttpParser.HttpParser parser = new HttpParser.HttpParser(HttpParser.ParserType.HTTP_RESPONSE);
            HttpParser.ParserSettings settings = new HttpParser.ParserSettings();
            settings.OnBody += new HttpParser.HttpDataCallback(delegate(HttpParser.HttpParser sender, MemoryStream stream, int pos, int len){
                stream.Seek(pos, SeekOrigin.Begin);
                GZipStream gzStream = new GZipStream(stream, CompressionMode.Decompress);
                using (MemoryStream decompressStream = new MemoryStream())
                {
                    gzStream.CopyTo(decompressStream);
                    json = Encoding.UTF8.GetString(decompressStream.ToArray());
                }
                return len;
            });
            parser.Execute(settings, new MemoryStream(payloadData));
            return json;
        }

        private TcpPacket SearchForTcpPacket(Packet p)
        {
            while (p != null)
            {
                if (p is EthernetPacket || p is IpPacket)
                {
                    p = p.PayloadPacket;
                }
                else if (p is TcpPacket)
                {
                    return p as TcpPacket;
                }
                else
                {
                    return null;
                }
            }

            return null;
        }

        private void RaiseEventOnUIThread(Delegate theEvent, params object[] args)
        {
            foreach (Delegate d in theEvent.GetInvocationList())
            {
                ISynchronizeInvoke syncer = d.Target as ISynchronizeInvoke;
                if (syncer == null)
                {
                    d.DynamicInvoke(args);
                }
                else
                {
                    syncer.BeginInvoke(d, args);  // cleanup omitted
                }
            }
        }

        #endregion
    }
}
